{
"trial_search" : "Fallsuche",
"trial_search_results" : "Ergebnisse der Fallsuche",
"trial_search_featured" : "Herausragender Fall",

"trial_search_current" : "Monentane Suche:",
"trial_search_add" : "Suchkriterien hinzufügen",

"trial_search_title" : "Titel",
"trial_search_language" : "Sprache",
"trial_search_authorId" : "Autoren-ID#",
"trial_search_authorName" : "Name des Autors",
"trial_search_featuredTrial" : "Herausragender Fall",
"trial_search_featuredTrial_isTrue" : "Fall ist herausragend",
"trial_search_featuredTrial_isFalse" : "Fall ist nicht herausragend",
"trial_search_sequenceId" : "Sequenz-ID#",
"trial_search_collabsName" : "Namen der Mitwirkenden",
"trial_search_collabsId" : "IDs# der Mitwirkenden",
"trial_search_authorOrCollabsId" : "ID# des Autors oder eines Mitwirkenden",

"trial_search_is" : "ist exakt",
"trial_search_isNot" : "ist nicht",
"trial_search_contains" : "enthält",
"trial_search_doesNotContain" : "enthält nicht",
"trial_search_has" : "enthält exakt",
"trial_search_doesNotHave" : "ohne",
"trial_search_hasValueContaining" : "mit",
"trial_search_doesNotHaveValueContaining" : "ohne",

"trial_search_private" : "Private Fälle miteinbeziehen",

"trial_search_page" : "Seite"
}
